'use strict'

const Model = use('Model')
const TournamentConstants = use('App/Constants/Tournament')
const utils = use('App/utils/functionHelpers')

const R = require('ramda')

class TournamentUser extends Model {
  static boot () {
    super.boot()
    this.addTrait('Tournament')
  }

  user () {
    return this.belongsTo('App/Models/User')
  }

  tournment () {
    return this.belongsTo('App/Models/Tournament')
  }

  static async checkIfPossibleStartFirstRound ({ tournamentId, needCountUser }) {
    const countUserTournament = await TournamentUser
      .query()
      .where('tournament_id', tournamentId)
      .where('round_name', TournamentConstants.FirstRound)
      .where('status', TournamentConstants.UserStatusWait)
      .getCount()
    return parseInt(countUserTournament, 10) === needCountUser
  }

  static async checkIfAllUserCompletedRound ({ tournamentId, round, countUsersPlaying }) {
    const countUserTournament = await TournamentUser
      .query()
      .where('tournament_id', tournamentId)
      .where('round_name', round)
      .where('status', TournamentConstants.UserStatusFinishedRound)
      .getCount()
    return parseInt(countUserTournament, 10) === countUsersPlaying
  }

  static async checkIfUserIsWinner ({ tournamentId, round, timeElapsed, maxCountRoundUser }) {
    const query = await TournamentUser
      .query()
      .select('socket_id', 'id', 'time_elapsed', 'user_id')
      .where('tournament_id', tournamentId)
      .andWhere('round_name', round)
      .andWhere('status', TournamentConstants.UserStatusFinishedRound)
      .fetch()

    const usersCompletedRound = query.toJSON()
    const compareElapsedData = { time: timeElapsed, userWon: false }

    const compareTimePlayers = usersCompletedRound.reduce((data, userCompleted) =>
      (userCompleted.time_elapsed > data.time ? {
        socketUserRemove: userCompleted.socket_id,
        time: userCompleted.time_elapsed,
        userWon: true,
        userLoseId: userCompleted.user_id,
        numberLess: true
      } : data), compareElapsedData)

    return usersCompletedRound.length < maxCountRoundUser ? {
      userWon: true,
      timeElapsed,
      numberLess: false
    } : compareTimePlayers
  }

  static async allUserOffline ({ tournamentId, round }) {
    const query = await TournamentUser
      .query()
      .with('user')
      .where('tournament_id', tournamentId)
      .andWhere('round_name', round)
      .fetch()
    const tournamentsUser = query.toJSON()
    const checkUserStatus = tournamentUser => !tournamentUser.user.online
    const filterArray = R.filter(checkUserStatus, tournamentsUser)
    const dataUser = user => ({
      username: utils.getParams(null, ['user', 'username'], user),
      offlineUserId: utils.getParams(null, ['user', 'id'], user),
      socketId: user.socket_id
    })
    return R.map(dataUser, filterArray)
  }


  static async checkOnlineRoundUser ({ ms, tournamentId, round }) {
    await utils.asyncTimeout(ms)
    return await TournamentUser.allUserOffline({ tournamentId, round })
  }
}

module.exports = TournamentUser
