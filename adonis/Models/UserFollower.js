'use strict'

const Model = use('Model')
const User = use('App/Models/User')

class UserFollower extends Model {
  static boot () {
    super.boot()
    this.addTrait('UserFollower')
    this.addHook('afterSave', 'UserFollowerHook.addActivity')
  }

  static get table () {
    return 'user_followers'
  }

  static async getFollowersList (userId) {
    return User
      .query()
      .select('users.id', 'users.username', 'user_followers.type', 'users.picture')
      .from('users')
      .innerJoin('user_followers', 'users.id', 'user_followers.follower_id')
      .where('user_followers.user_id', userId)
      .andWhere('user_followers.type', UserFollower.TYPE_FOLLOW)
      .orderBy('users.username')
  }

  static async getFollowedList (userId) {
    return User
      .query()
      .select('users.id', 'users.username', 'user_followers.type', 'users.picture')
      .from('users')
      .innerJoin('user_followers', 'users.id', 'user_followers.user_id')
      .where('user_followers.follower_id', userId)
      .andWhere('user_followers.type', UserFollower.TYPE_FOLLOW)
      .orderBy('users.username')
  }

  static async getType (userId, followerId) {
    const userFollower = await UserFollower.query()
      .where('user_id', userId)
      .andWhere('follower_id', followerId)
      .first()
    return userFollower ? userFollower.type : UserFollower.TYPE_UNFOLLOW
  }

  static async getFirstPendingFollower (userId) {
    const follower = await UserFollower.query()
      .select('users.id', 'users.username', 'users.picture')
      .from('user_followers')
      .where('user_followers.user_id', userId)
      .where('user_followers.type', UserFollower.TYPE_PENDING)
      .leftJoin('users', 'user_followers.follower_id', 'users.id')
      .orderBy('user_followers.updated_at')
      .first()

    return follower
  }
}

module.exports = UserFollower
