'use strict'

const got = require('got')
const randomstring = require('randomstring')

const Hash = use('Hash')
const Model = use('Model')
const UserProfile = use('App/Models/UserProfile')
const Drive = use('Drive')
const Helpers = use('Helpers')

class User extends Model {
  static get hidden () {
    return ['password']
  }
  static boot () {
    super.boot()
    this.dirName = 'uploads/userPictures/'

    this.addHook('beforeCreate', async userInstance => {
      if (userInstance.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })

    this.addHook('beforeSave', async userInstance => {
      if (userInstance.picture
        && (userInstance.picture.indexOf('http://') > -1
          || userInstance.picture.indexOf('https://') > -1)) {
        try {
          const res = await got(userInstance.picture, { encoding: null })
          const fileName = `avatar_${randomstring.generate(16)}.jpg`
          const savedFile = await Drive.put(User.getDirPath() + fileName, Buffer.from(res.body))
          if (savedFile) {
            userInstance.picture = fileName
          }
        } catch (error) {
          console.log(error.response.body)
        }
      }
    })

    this.addHook('afterCreate', async userInstance => {
      await UserProfile.create({ user_id: userInstance.id })
    })

    this.addTrait('SettingsUser')
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  profile () {
    return this.hasOne('App/Models/UserProfile')
  }

  tournaments () {
    return this
      .belongsToMany('App/Models/Tournament')
      .pivotTable('tournament_users')
      .withTimestamps()
  }

  static getDirPath () {
    return Helpers.publicPath(`${User.dirName}`)
  }
}

module.exports = User
