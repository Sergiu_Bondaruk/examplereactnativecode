'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')
const Role = use('App/Constants/Role')
const config = use('App/Constants/Options')
const errorKeys = use('App/Constants/ErrorKeys')

class UserController {
  async getAll ({ request, response }) {
    const query = request.get(['_end', '_start', '_sort', '_order'])

    const users = await User.query()
      .where('role', '!=', Role.player)
      .offset(query._start)
      .limit(query_end)
      .orderBy(query._sort, query._order)

    const usersCount = await User.query().where('role', '!=', Role.player).getCount()

    response.safeHeader('X-Total-Count', usersCount)
    return response.json(users.toJSON())
  }

  async user ({ response, params }) {
    const user = await User.find(params.id)
    return response.json(user)
  }

  async update ({ request, response, params }) {
    const user = await User.find(params.id)
    const dataUser = request.only(['username', 'email', 'role', 'password', 'status'])

    user.username = dataUser.username
    user.email = dataUser.email
    user.role = dataUser.role
    user.status = dataUser.status
    user.password = dataUser.password

    await user.save()
    return response.json(user)
  }

  async create ({ request, response, auth }) {
    const dataUser = request.only(['username', 'email', 'role', 'password', 'status'])
    try {
      const user = await User.create({
        username: dataUser.username,
        email: dataUser.email,
        role: dataUser.role,
        status: dataUser.status,
        password: dataUser.password
      })
      const token = await auth.generate(user)
      return response.json({
        user,
        token
      })
    } catch (e) {
      console.log(e)
    }
  }

  async login ({ request, response, auth }) {
    const dataLogin = request.only(['username', 'password'])
    try {
      const user = await User.findBy('username', dataLogin.username)
      const correct = await Hash.verify(dataLogin.password, user.password)
      if (correct) {
        if (user.status) {
          const token = await auth.generate(user)
          return response.json({
            token,
            status: 201,
            role: user.role,
            username: dataLogin.username
          })
        }
        return response.json({
          status: 401,
          error: errorKeys.blockedAccount
        })
      }
      return response.json({
        status: 401,
        error: errorKeys.errorLoginData
      })
    } catch (e) {
      return response.unauthorized({ error: e.message })
    }
  }

  async loginViaFacebook ({ request, response, auth }) {
    const dataUser = request.only(['email', 'username', 'facebook_id', 'picture'])
    let userFacebook = await User.checkFacebookAccount(dataUser.facebook_id)
    if (!userFacebook) {
      if (dataUser.email) {
        const userAlreadyRegistered = await User.checkEmailUsed(dataUser.email)
        if (userAlreadyRegistered) {
          userAlreadyRegistered.facebook_id = dataUser.facebook_id
          await userAlreadyRegistered.save()
          userFacebook = userAlreadyRegistered
        } else {
          userFacebook = await User
            .createUser(dataUser.email, dataUser.username, null, dataUser.facebook_id, Role.player, dataUser.picture)
        }
      } else {
        userFacebook = await User
          .createUser(null, dataUser.username, null, dataUser.facebook_id, Role.player, dataUser.picture)
      }
    }
    if (userFacebook.status === config.Disabled) {
      return response.json({
        status: 400,
        error: errorKeys.blockedAccount
      })
    }

    try {
      const token = await auth.generate(userFacebook)
      return response.json({
        status: 200,
        user: userFacebook,
        token
      })
    } catch (e) {
      return response.json({ error: e.message })
    }
  }

  async delete ({ response, params }) {
    const user = await User.find(params.id)
    try {
      await user.delete()
      return response.json(true)
    } catch (e) {
      return response({ error: e.message })
    }
  }

  async checkEmailAlreadyUse ({ request, response }) {
    const data = request.only(['email'])
    const user = await User.checkEmailUsed(data.email)
    if (user) {
      return response.status(401).json({ validation: false })
    }
    return response.status(200).json({ validation: true })
  }
}

module.exports = UserController
