'use strict'

const Options = use('App/Constants/Options')
const TournamentConstants = use('App/Constants/Tournament')

const Tournament = use('App/Models/Tournament')
const TournamentUser = use('App/Models/TournamentUser')
const TournamentResource = use('App/Models/TournamentResource')
const CompareImage = use('App/Models/CompareImage')

class TournamentController {
  async getAllTournaments ({ request, response }) {
    let { typeGame } = request.only(['typeGame'])

    if (typeGame === Options.Sudoku) {
      typeGame = TournamentConstants.SudokuGameType
    } else if (typeGame === Options.Spot) {
      typeGame = TournamentConstants.DifferenceGameType
    }

    let tournaments = await Tournament.all()
    if (typeGame) {
      tournaments = await Tournament.query()
        .where('game_type', '=', typeGame)
    }

    return response.json(tournaments)
  }
  async getRoomTournamentInfo ({ request, response, auth }) {
    const { tournamentId, round } = request.only(['tournamentId', 'round'])
    const tournamentData = await TournamentUser.getCountTournamentUsers({ tournamentId, round })
    const user = await auth.getUser()
    const join = await TournamentUser.checkUserSubscribeOtherTournament(user.id, tournamentId)
    return response.json({
      tournamentData,
      join
    })
  }

  async getTournamentTable ({ request, response, auth }) {
    const { tournamentId, round } = request.only(['tournamentId', 'round'])
    const user = await auth.getUser()
    const tournamentUsers = await TournamentUser.getUsersTournament({
      tournamentId,
      round,
      userId: user.id
    })
    return response.json(tournamentUsers)
  }

  async getOnlineSpotImages ({ request, response }) {
    const { tournamentId, round } = request.only(['tournamentId', 'round'])
    const query = await TournamentResource
      .query()
      .where('tournament_id', tournamentId)
      .andWhere('round', round)
      .with('compareImages')
      .first()

    const data = query.toJSON()
    data.compareImages.basic_image = CompareImage.getAbsolutePath(data.compareImages.basic_image)
    data.compareImages.compare_image = CompareImage.getAbsolutePath(data.compareImages.compare_image)
    return response.json(data)
  }

  async getSudokuPuzzle ({ request, response }) {
    const { tournamentId, round } = request.only(['tournamentId', 'round'])
    const data = await TournamentResource
      .query()
      .where('tournament_id', tournamentId)
      .andWhere('round', round)
      .first()
    return response.json(data)
  }

  async changeUserStatus ({ request, response, auth }) {
    const { tournamentId, status } = request.only(['tournamentId', 'status'])
    const userData = await auth.getUser()
    const updateStatusUser = await TournamentUser.changeUserStatus({
      tournamentId,
      userId: userData.id,
      status
    })
    return response.status(201).json({ updateStatusUser })
  }
}

module.exports = TournamentController
